/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.ua.estructura.pila;

/**
 * Pila de enteros
 * @author fersa
 */
public class PilaVector {
    //El análogo a NUMAX, que en JAVA se comienza desde cero, por eso es N-1 NUMAX
    private static final int numMax = 20;
    private int cima;
    private int [] listaPila;
    
    public PilaVector(){
        this.cima = -1;
        this.listaPila = new int[this.numMax];
    }

    public void setCima(int cima) {
        this.cima = cima;
    }

    public void setListaPila(int[] listaPila) {
        this.listaPila = listaPila;
    }

    public static int getNumMax() {
        return numMax;
    }

    public int getCima() {
        return cima;
    }

    public int[] getListaPila() {
        return listaPila;
    }
    
    
    //Metodo para evaluar si está vacía la pila.
    public boolean esVacia(){
       return this.cima == -1; 
    }
    //Metodo para evaluar si está llena la pila.
    public boolean estaLlena(){
        return this.cima == this.numMax-1;
    }
    
    //Metodo para retornar el elemento que se encuentra en la cima de la pila.
    public int valorCima() throws Exception{
        if(this.esVacia())
        {
            throw new Exception ("Pila Vacía, no se puede obtener el valor.\n");
        }
        return this.listaPila[this.cima];
    }
    
    //Metodo para apilar
    public void apilar(int elemento) throws Exception{
        if(this.estaLlena()){
            throw new Exception ("La pila está llena, no se puede cargar más valores.\n");
        }
        //incrementamos en uno la cima
        this.cima++;
        //apilamos el valor.
        this.listaPila[this.cima] = elemento;
    }
    
    //Metodo para desapilar en la pila.
    public int desapilar() throws Exception{
        if (this.esVacia()){
            throw new Exception("Pila vacía, no se puede desapilar nada, ya que no hay nada.\n");
        }
        int auxiliar = this.listaPila[cima];
        this.cima--;
        return auxiliar;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
